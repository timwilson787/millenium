var i = $('.carousel .item'); 
var h = $(window).height();
var w = $(window).width();
i.eq(0).addClass('active');
i.height(h); 
i.addClass('full-screen');

$('.carousel img').each(function() {
  var s = $(this).attr('src');
  var c = $(this).attr('data-color');
  $(this).parent().css({
    'background-image' : 'url(' + s + ')',
    'background-color' : c
});
  $(this).remove();
});

$(window).on('resize', function (){
  h = $(window).height();
  i.height(h);
});

$(window).on('load resize', function (){
    if($(w) > 768){
        $('.carousel').carousel({
          interval: 4000,
          pause: "true"
      });
        $(".mobile-menu").hide();
    }
});

$(".js-menu").click(function(e){
e.preventDefault();
    $(".mobile-menu").slideToggle();
    $(this).toggleClass("fa-close");
    return false;
});

$(".sub-menu").click(function(f){
f.preventDefault();
    $(".m-nav-sub-1").slideToggle();
    return false;

});